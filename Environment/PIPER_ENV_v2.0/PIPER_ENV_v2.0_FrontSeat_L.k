$ Copyright (C) 2017 TU Berlin
$
$ This file is part of the PIPER Environment Models (PIPER ENV).
$
$ PIPER ENV is free software: you can redistribute it and/or modify
$ it under the terms of the GNU General Public License as published by
$ the Free Software Foundation, either version 3 of the License, or
$ (at your option) any later version.
$
$ PIPER ENV is distributed in the hope that it will be useful,
$ but WITHOUT ANY WARRANTY; without even the implied warranty of
$ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
$ GNU General Public License for more details.
$
$ You should have received a copy of the GNU General Public License
$ along with the PIPER ENV. If not, see <http://www.gnu.org/licenses/>.
$
$ According to the term of the GPL v3 article 7, the Licenser hereby agrees to
$ include to GPL v3 the following additional terms:
$ - in no event, unless required by applicable law, shall any copyright holder,
$ or any other third party who modifies and/or conveys the model as permitted
$ in accordance with this legal license, be liable to any other Party or third
$ party for any indirect or direct damages, including but not limited to death,
$ bodily injury and property damages (public or private), even if such holder
$ or other party has been advised of the possibility of such damages. Any use
$ of the model is at your own risk.
$
$ This work has received funding from the European Union Seventh Framework
$ Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
$
$ Contributors include:
$ Stefan Kirscht, Ahmed Saeed, William Goede, Martin Sitte [TU Berlin]
$
*KEYWORD
$
$    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$    #                                                                         #
$    #    PIPER Project (Grant agreeement no. 605544)                          #
$    #                                                                         #
$    #    Technische Universitšt Berlin                                        #
$    #    Fachgebiet Kraftfahrzeuge (KFZB)                                     #
$    #                                                                         #
$    #    PIPER Vehicle Environment Model                                      #
$    #    Model Version: 2.0                                                   #
$    #    Assembly: Front Seat Left                                            #
$    #    Units:         S3 (mm - ms - kg - kN)                                #
$    #                                                                         #
$    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$
*PARAMETER
$ The local assembly parameters of the VEM are organised with the following syntax:
$ * Assembly        2 letters:     fl=floor; fs=front seat; rs=rear seat; ip=instrument panel; rf=roof; ss=side structure
$ * Component       2 letter:      gp=gas pedal; fe=front extension; ml= main lower floor; mt=middle tunnle; se=side extension;
$                                  a1= belt attachment (number 1-2 for ISOFIX attachments); bl= belt lock; hr= head rest; sb= seat back; sc=seat cushion;
$                                  da=Dashboard; sw= steering wheel;
$                                  ws=windscreen; ro=roof;
$      											   		 fw= front window; rw=rear window; pi= pillars; fd= front door; rd= rear door; fs= front window sill; rs= rear window sill
$ * Location        1 letter:      l= left; r=right; ml=middle left; mr=middle right
$ * Parameter Type  1 letter:      s=scale; t=translation; r=rotation
$ * Direction       1 letter:      x; y; z
$
$
$
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$ # # # # #     PARAMETERS      # # # # # # # # # # # # # # # # # # # # # # # #
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$
$_______________GAS PEDAL_______________________________________________________
$
$------------------------------------Scaling------------------------------------
R flgpsx  1         R flgpsy  1         R flgpsz  1
$----------------------------------Translation----------------------------------
R flgptx  0         R flgpty  0         R flgptz  0
$-----------------------------------Rotation------------------------------------
R flgpry  0
$
$
$_______________SEAT BACKREST___________________________________________________
$
$------------------------------------Scaling------------------------------------
R fssblsx 1         R fssblsy 1         R fssblsz 1
$----------------------------------Translation----------------------------------
R fssbltx 0         R fssblty 0         R fssbltz 0
$-----------------------------------Rotation------------------------------------
R fssblry 0
$
$
$_______________SEAT CUSHION____________________________________________________
$
$------------------------------------Scaling------------------------------------
R fssclsx 1         R fssclsy 1         R fssclsz 1
$----------------------------------Translation----------------------------------
R fsscltx 0         R fssclty 0         R fsscltz 0
$-----------------------------------Rotation------------------------------------
R fssclry 0
$
$
$_______________SEAT HEADREST___________________________________________________
$
$------------------------------------Scaling------------------------------------
R fshrlsx 1         R fshrlsy 1         R fshrlsz 1
$----------------------------------Translation----------------------------------
R fshrltx 0         R fshrlty 0         R fshrltz 0
$
$
$_______________BELT LOCK_______________________________________________________
$
$------------------------------------Scaling------------------------------------
R fsbllsx 1         R fsbllsy 1         R fsbllsz 1
$----------------------------------Translation----------------------------------
R fsblltx 0         R fsbllty 0         R fsblltz 0
$-----------------------------------Rotation------------------------------------
R fsbllry 0         R fsbllrz 0
$
$
$_______________BELT ATTACHMENT 01______________________________________________
$
$------------------------------------Scaling------------------------------------
R fsa1lsx 1         R fsa1lsy 1         R fsa1lsz 1
$----------------------------------Translation----------------------------------
R fsa1ltx 0         R fsa1lty 0         R fsa1ltz 0
$
$
$_______________BELT ATTACHMENT 02______________________________________________
$
$------------------------------------Scaling------------------------------------
R fsa2lsx 1         R fsa2lsy 1         R fsa2lsz 1
$----------------------------------Translation----------------------------------
R fsa2ltx 0         R fsa2lty 0         R fsa2ltz 0
$
$
$
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$ # # # # #     COMPONENT TRANSFORMATIONS     # # # # # # # # # # # # # # # # #
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$
*DEFINE_TRANSFORMATION
$_______________GAS PEDAL_______________________________________________________
$#  tranid
      1002
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &flgpsx   &flgpsy   &flgpsz          0.0       0.0       0.0       0.0
POINT          10020  1742.032    339.62  124.4783
POINT          10021  1742.022    194.62  124.4586
ROTATE         10021     10020&flgpry
TRANSL    &flgptx   &flgpty   &flgptz          0.0       0.0       0.0       0.0
*DEFINE_TRANSFORMATION
$_______________SEAT BACKREST___________________________________________________
$#  tranid
      1100
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fssblsx  &fssblsy  &fssblsz         0.0       0.0       0.0       0.0
POINT          11000   685.031   127.334   177.909
POINT          11001   685.031   590.584   177.909
ROTATE         11000     11001&fssblry
TRANSL    &fssbltx  &fssblty  &fssbltz         0.0       0.0       0.0       0.0
$
*DEFINE_TRANSFORMATION
$_______________SEAT CUSHION____________________________________________________
$#  tranid
      1101
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fssclsx  &fssclsy  &fssclsz         0.0       0.0       0.0       0.0
POINT          11010   709.007   235.294   120.247
POINT          11011   709.007   492.439   120.247
ROTATE         11010     11011&fssclry
TRANSL    &fsscltx  &fssclty  &fsscltz         0.0       0.0       0.0       0.0
$
*DEFINE_TRANSFORMATION
$_______________SEAT HEADREST___________________________________________________
$#  tranid
      1102
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fshrlsx  &fshrlsy  &fshrlsz         0.0       0.0       0.0       0.0
POINT          11000   685.031   127.334   177.909
POINT          11001   685.031   590.584   177.909
ROTATE         11000     11001&fssblry
TRANSL    &fshrltx  &fshrlty  &fshrltz         0.0       0.0       0.0       0.0
$
*DEFINE_TRANSFORMATION
$_______________BELT LOCK_______________________________________________________
$#  tranid
      1103
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fsbllsx  &fsbllsy  &fsbllsz         0.0       0.0       0.0       0.0
POINT          11030   715.722   190.836   178.924
POINT          11031   715.722   192.836   178.924
ROTATE         11030     11031&fsbllry
POINT          11032   715.722   190.836   178.924
POINT          11033   715.722   190.836   180.924
ROTATE         11032     11033&fsbllrz
TRANSL    &fsblltx  &fsbllty  &fsblltz         0.0       0.0       0.0       0.0
$
*DEFINE_TRANSFORMATION
$_______________BELT ATTACHMENT 01______________________________________________
$#  tranid
      1104
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fsa1lsx  &fsa1lsy  &fsa1lsz         0.0       0.0       0.0       0.0
TRANSL    &fsa1ltx  &fsa1lty  &fsa1ltz         0.0       0.0       0.0       0.0
$
*DEFINE_TRANSFORMATION
$_______________BELT ATTACHMENT 02______________________________________________
$#  tranid
      1105
$#  option        a1        a2        a3        a4        a5        a6        a7
SCALE     &fsa2lsx  &fsa2lsy  &fsa2lsz         0.0       0.0       0.0       0.0
TRANSL    &fsa2ltx  &fsa2lty  &fsa2ltz         0.0       0.0       0.0       0.0
$
$
$
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$ # # # # #     COMPONENT INCLUDES      # # # # # # # # # # # # # # # # # # # #
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$
*INCLUDE
PIPER_ENV_v2.0_FL_L_MainFloor.k
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FL_GasPedal.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1002
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_L_SeatBackrest.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1100
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_L_SeatCushion.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1101
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_L_SeatHeadrest.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1102
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_L_BeltLock.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1103
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_L_BeltAttachment1.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1104
$
*INCLUDE_TRANSFORM
PIPER_ENV_v2.0_FS_L_BeltAttachment2.k
$#  idnoff    ideoff    idpoff    idmoff    idsoff    idfoff    iddoff
         0         0         0         0         0         0         0
$#  idroff
         0
$#  fctmas    fcttim    fctlen    fcttem   incout1    unused
       1.0       1.0       1.01.0                1
$#  tranid
      1105
$
$
$
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$ # # # # #     ASSEMBLY CONSTRAINTS    # # # # # # # # # # # # # # # # # # # #
$ # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
$
*CONSTRAINED_RIGID_BODIES
$ Floor Assembly
      1000      1001         0
      1000      1003         0
$ Belt Buckle Assembly
      1109      1108         0
*BOUNDARY_SPC_SET_ID
$#      id                                                               heading
      1106Constraint_FS_L_BeltAttachment1
$#    nsid       cid      dofx      dofy      dofz     dofrx     dofry     dofrz
      1106         0         1         1         1         1         1         1
$
$#      id                                                               heading
      1107Constraint_FS_L_BeltAttachment2
$#    nsid       cid      dofx      dofy      dofz     dofrx     dofry     dofrz
      1107         0         1         1         1         1         1         1
$
$#      id                                                               heading
      1110Constraint_FS_L_BeltLashJoint
$#    nsid       cid      dofx      dofy      dofz     dofrx     dofry     dofrz
      1110         0         1         1         1         1         1         1
*CONSTRAINED_EXTRA_NODES_SET
$#     pid      nsid     iflag
      1108      1111         0
*END
